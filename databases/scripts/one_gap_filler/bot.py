from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from time import sleep

class Bot:
    def __init__(self):
        self.open_window()

    def open_window(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Chrome(options=options)
        self.driver.set_window_size(350, 400)

        with open('one-gap.txt') as gap_file:
            for i in gap_file.readlines():
                i = str(i)
                self.url = 'http://bustabit.com/game/' + str(i)
                self.driver.get(self.url)

                try:
                    WebDriverWait(self.driver, 60).until(
                        EC.presence_of_element_located((By.CLASS_NAME, "bold"))
                    )
                    elements = self.driver.find_elements_by_class_name("bold")
                    print(i.replace('\n', '') + ',' + elements[0].text)

                    next_game_button = self.driver.find_element_by_link_text(
                        '← Prev Game')
                    next_game_button.click()
                except:
                    self.driver.quit()
                    continue
                sleep(5)
        self.driver.quit()


bot = Bot()
