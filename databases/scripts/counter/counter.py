import sys, csv

target_payout = float(sys.argv[1])
with open('with_gaps.csv') as csv_file:
    skip_file = open('skips.txt', 'w')

    csv_reader = csv.reader(csv_file, delimiter=',')
    result = {}
    result_with_gid = {}
    counter = 0
    last_gid = 0
    for row in csv_reader:
        try:
            gid = int(row[0])
            payout = float(row[1].replace('x', ''))
        except:
            print('problem on: ',row)
            break
        if not last_gid:
            last_gid = gid

        if last_gid != gid - 1:
            print("======================")
            print("GAP FOUND! SKIPPING...")
            print("on game_id: ", gid)
            skip_file.write(str(gid) + '\n')
            counter = 0
            last_gid = gid
            continue

        if payout < target_payout:
            counter += 1

        elif counter > 0 and payout >= target_payout:
            if counter in result:
                result[counter]['count'] += 1
                result_with_gid[counter]['count'] += 1
                result_with_gid[counter]['last_gid'].append(last_gid)
            else:
                result[counter] = {'count': 1}
                result_with_gid[counter] = {'count': 1, 'last_gid': [last_gid]}
            counter = 0

        last_gid = gid
with open('result.txt', 'w') as output:
    output.write('Result: ' + str(sorted(result_with_gid.items())))
print('Result: ', sorted(result.items()))
