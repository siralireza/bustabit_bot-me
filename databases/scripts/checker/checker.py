import csv

with open('with_gaps_reverse.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    last_gid = 0
    last_payout = 0

    one_file = open('one-gap.txt', 'w')
    more_file = open('more-gap.txt', 'w')
    equal_file = open('equal-ids.txt', 'w')

    duplicates = set()
    games = []
    for row in csv_reader:
        try:
            gid = int(row[0])
            payout = row[1]
            games.append({'gid': gid, 'payout': payout})
        except:
            print(row)
    for i, item in enumerate(games):
        gid = item['gid']
        payout = item['payout']

        if not last_gid:
            last_gid = gid
            last_payout = payout
            continue

        if last_gid == gid:
            equal_file.write(str(gid) + '\n')
            duplicates.add(gid)
            continue

        if last_gid == gid + 1:
            last_gid = gid
            continue

        if last_gid == gid + 2:
            one_file.write(str(gid + 1) + '\n')
            # print('just one gap: ', gid + 2)
            last_gid = gid
            continue

        counter = gid + 1
        # print('gid: ', gid)
        # print('start: ', counter)
        more_file.write(str(counter) + '-')
        start = counter

        while last_gid >= counter:
            # print(counter)
            counter += 1

        end = counter
        more_file.write(str(counter) + '-' + str(end - start) + '\n')

        # print('end: ', counter - 1)
        # print('last_gid: ', last_gid)

        last_gid = gid

    with open('new-file.csv', 'w') as new_csv_file:
        for item in games:
            if item['gid'] in duplicates:
                continue
            new_csv_file.write(str(item['gid']) + ',' + str(item['payout']) + '\n')
        new_csv_file.close()
csv_file.close()
