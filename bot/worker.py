from bot import Bot
import threading
from env import threads_count

for i in range(threads_count):
    print('starting thread: ' + str(i + 1))
    bot = Bot(i)
    print('starting gathering')
    t = threading.Thread(target=bot.start_bot)
    t.start()
