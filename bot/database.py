from peewee import DoesNotExist

from model import db, Game


class ORM:
    def __init__(self):
        db.connect()
        db.create_tables([Game], safe=True)

    def check_game_exist(self, game_id):
        try:
            return True if Game.get(Game.game_id == game_id) else False
        except DoesNotExist:
            return False

    def save_game(self, game_id, bust, date):
        return Game.create(game_id=game_id, bust=bust, date=date)

