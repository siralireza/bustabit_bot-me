from env import totalGames, eachRange

total = totalGames
eachRange = eachRange


def get_range(id=0):
    start = total - (id * eachRange)
    stop = start - eachRange
    return start if id == 0 else start - 1, stop
