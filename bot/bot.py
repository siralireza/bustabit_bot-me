import threading
import gc

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

from database import ORM
from ranges import get_range
from time import sleep
from env import *

db = ORM()


class Bot:
    def __init__(self, thread_id):
        sleep(thread_id)
        self.thread_id = thread_id
        self.open_window()

    def open_window(self, start_range=None):
        if headless:
            options = Options()
            options.headless = True
            self.driver = webdriver.Chrome(options=options)
        else:
            self.driver = webdriver.Chrome()

        self.driver.set_window_size(350, 400)

        if start_range:
            self.start, self.stop = int(
                start_range), int(start_range) - eachRange
        else:
            self.start, self.stop = get_range(self.thread_id)

        self.url = gamesUrl + str(self.start)

        print('getting first url...')
        self.driver.get(self.url)

    def start_bot(self):
        rounds = 0
        i = self.start
        print('Ranges:', self.start, self.stop)
        while i >= self.stop and i <= self.start:
            print("=========THREAD {}========".format(str(self.thread_id + 1)))
            g = threading.Thread(target=gc.collect)
            g.start()
            print('freeing memory...')

            game_id = str(i)
            try:
                print('starting bot with game: ' + game_id)
                WebDriverWait(self.driver, timeout).until(
                    EC.presence_of_element_located((By.CLASS_NAME, "bold"))
                )
                print('element has been seen!')
                elements = self.driver.find_elements_by_class_name("bold")
                print(
                    game_id + '- ' + elements[0].text + ' - ' + elements[1].text)
                print('checking database')
                if not db.check_game_exist(game_id):
                    print('saving to db as thread...')
                    t = threading.Thread(target=db.save_game, args=(
                        game_id, elements[0].text, elements[1].text,))
                    t.start()
                    print('thread started...')
                next_game_button = self.driver.find_element_by_link_text(
                    '← Prev Game')
                next_game_button.click()
                print('going to next game...')
                if rounds >= rounds_to_reopen:
                    rounds = 0
                    print('reopen browsers')
                    raise Exception()
                else:
                    rounds += 1
                i += direction
            except:
                self.driver.quit()
                sleep(sleeping_time)
                print('opening window again...')
                self.open_window(start_range=i)
                continue

        self.driver.quit()
        print('FINISHED - '+game_id)
