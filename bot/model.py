from peewee import *

db = SqliteDatabase('games.db')


class Game(Model):
    id = BigAutoField()
    game_id = BigIntegerField()
    bust = CharField()
    date = CharField()

    class Meta:
        database = db
